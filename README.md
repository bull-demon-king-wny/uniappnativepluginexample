﻿# - uni配网插件示例工程使用说明

## 简介
本项目是一个uniAPP原生插件测试工程项目，主要用来对插件 *乐鑫(esp芯片)蓝牙配网插件*  和 *乐鑫(esp芯片)UDP配网插件* 的进行功能展示和测试。插件的获取，可在uniAPP原生插件市场搜索。
## 使用说明
  ### 插件工程运行测试依赖环境
  - HBuilder X 编译工具（本工程测试时使用的版本是3.6.15，建议不低于此版本）
  - 原生插件 *乐鑫(esp芯片)蓝牙配网插件*  或 *乐鑫(esp芯片)UDP配网插件*。
  - 乐鑫模组芯片产品（嵌入式产品），用于配网功能测试。（产品应遵循乐鑫SDK中 *ESPTouch v2* 或 *wifi_prov_scheme_ble* 对应的配网规范）。
  - 安卓或IOS手机。
 
  ### 整体流程
  1. 下载测试工程示例，并在 *HBuilder X 编译工具* 中打开。
  2. 配置原生插件。[官方指南，点击查看](https://nativesupport.dcloud.net.cn/NativePlugin/use/use.html)
  3. 配置其他参数， 打包运行测试。
  ### 其他问题
   - 打包运行插件，所需要的权限配置， 参见对应插件的使用说明 - **打包时必要的权限配置** 章节。
   -  本项目在权限申请环节，用到了uni官方提供的权限申请插件 *permission.js* 。 [详见官方说明](https://ext.dcloud.net.cn/plugin?id=594)

# - 乐鑫(esp芯片)UDP配网插件使用说明
## 简介
 本插件主要为使用 *乐鑫(esp芯片) 的嵌入式产品* 提供手机端的配网API。需要注意嵌入式产品需遵循乐鑫SDK *ESPTouch v2版协议* 才可正常使用。
## 功能特性
- 支持 最低 *安卓-6.0* 和 *IOS-9.0* 。
- 支持 获取当前手机连接的WIFI信息。
- 支持 乐鑫 *ESPTouch v2* 版协议的配网。
- 支持 AES秘钥。
- 支持在配网时发送 自定义数据。
- 支持安卓和IOS插件运行所需权限，申请弹窗逻辑功能。
- **提供测试工程项目示例**，支持vue页面和nvue页面，可直接复制对应页面代码到自己的工程中，简单修改，即可正常测试使用。或者直接下载测试工程项目（若您当前在uni插件市场对应的插件说明页面，可直接在页面右上角 点击 **使用HBuiderX 导入示例项目**），简单配置，即可测试运行。*具体使用说明，详见工程代码示例中的  **README.md** 文件*。

## 示例工程项目源码地址
 [点击跳转示例项目源码地址](https://gitee.com/bull-demon-king-wny/uniappnativepluginexample?_from=gitee_search)
## 插件示例工程 配网页面 流程
  ```mermaid
graph LR
A(初始化配网环境)  --> B[配网使用说明] --> C[开始配网]-->D[环境清理]
```
- 初始化配网环境: 主要检查配网所需手机权限是否正常。若不正常，则弹窗申请。
- 配网使用说明：页面文本展示，主要告知用户配网操作流程和注意事项。
- 开始配网： 发送UDP配网数据包。
- 环境清理：主要用来清理配网插件运行时申请的内存或未完成的任务。


## API使用说明
### 引入--使用API之前需要先引入原生插件
```
var espModule = uni.requireNativePlugin("nw-udp-esp-provisioning_v2_module")
```
### 使用
####  API-初始化配网环境:

```
let objectData = {"isRequestPermissions":true};
var code = espModule.udpEnvironmentOnLoad(objectData);
```

> 入参: 
> let objectData = {"isRequestPermissions":true};
> isRequestPermissions： Bool值， 为true或false。 true：使用插件内的位置权限弹窗申请。 fasle：不使用插件内的位置权限弹窗申请。（鉴于安卓系统位置权限申请的逻辑问题，此值的抉择，可参考如下：若APP的其他功能模块也需要位置权限，则选择false，并在APP使用本模块之前，引导用户完成申请位置权限的功能。若APP其他模块没有使用位置权限，建议为true。 ）

> 出参：
> var code： Number值，具体参见 **同步API返回code码含义** 。

> 异步回调：有
> 具体参见 **API异步返回值含义** 。

####  API-开始配网:
```
let uWifiInputInfo:{
				"password":"", 
				"aes":"", 
				"data":"" 
				}
var code =espModule.udpStartProvisioning(uWifiInputInfo);
```
> 入参: 
> let uWifiInputInfo: { "password":"", "aes":"","data":"" }
> password:  WIFI的密码
> aes:  AES秘钥 与设备匹配,不需要时传"" 只允许为"" 或 长度为16的字符串 建议写死
> data:  传递给设备的自定义数据. 不需要时传""

> 出参：
> var code： Number值，具体参见 **API同步返回code码含义** 。
> 配网有超时结束，最长90秒。

> 异步回调：有
> 具体参见 **API异步返回值含义** 。

####  API-停止配网:
```
var code = espModule.udpStopProvisioning();
```
> 出参：
> var code： Number值，具体参见 **API同步返回code码含义** 。

> 异步回调：有
> 具体参见 **API异步返回值含义** 。
####  API-环境清理:
```
var code = espModule.udpEnvironmentOnUnload();
```
> 出参：
> var code： Number值，具体参见 **API同步返回code码含义** 。

> 异步回调：有
>具体参见 **API异步返回值含义** 。

### API同步返回code码含义
```
let codeState = [
		{
			code:0,
			message:"执行成功!"
			
		},
		{
			code:1,
			message:"环境已存在,不需要重复初始化!"
			
		},
		{
			code:2,
			message:"provisioner供应器未得到初始化,请先执行环境初始化!"
			
		},
		{
			code:3,
			message:"provisioner数据同步任务,已经停止运行,无需重复停止!"
			
		},
		{
			code:4,
			message:"provisioner数据同步任务,已经在运行,无需重复运行!"
			
		},
		{
			code:5,
			message:"provisioner数据供应任务,已经停止运行,无需重复停止!"
			
		},
		{
			code:6,
			message:"provisioner数据供应任务,已经在运行,无需重复运行!"
			
		},
		{
			code:7,
			message:"参数传递错误!"
			
		},
		{
			code:8,
			message:"预留!"
			
		},
		{
			code:9,
			message:"预留!"
			
		},
		{
			code:10,
			message:"环境检查OK!"  //插件使用者无需关注
			
		},
		{
			code:11,
			message:"位置权限未打开!" // 参数isRequestPermissions 为false有效
			
		},
		{
			code:12,
			message:"GPS未打开!"
			
		},
		{
			code:13,
			message:"WIFI未连接!"
			
		},
		{
			code:14,
			message:"连接的是5G WI-FI,设备只支持2.4G WI-FI!"
			
		},
		/* 需要注意 若参数isRequestPermissions为true, 请保证不要在系统的其他地方调用 申请位置权限弹窗的操作,否则回应码将是不准确的! */
		{
			code:15,
			message:"需要位置权限,才能继续!" //参数isRequestPermissions 为true有效  (第一次弹出的位置权限弹窗,被用户手动拒绝了,需要手动引导用户)
			
		},
		{
			code:16,
			message:"请允许位置权限,才能继续!" //参数isRequestPermissions 为true有效 ( 位置权限第一次弹窗成功,等待用户操作后的回调)
			
		},
		{
			code:17,
			message:"不准确的!--理论上不会出现这个回应码,若出现则说明系统在其他位置也调用了申请位置权限弹窗的操作!" //参数isRequestPermissions 为true有效
			
		}
		
	]
```
### API异步返回值含义
#### 注册回调事件
注册回调事件，才能收到异步返回值。**建议 直接 参考示例项目页面代码**。
```
var globalEvent = uni.requireNativePlugin('globalEvent'); //插件全局监听

/* 注册插件监听事件,页面内有效 */
globalEvent.addEventListener('espEvent', function(event) {
		console.log('收到插件espEvent事件:'+JSON.stringify(event));
	 });
```
#### 返回值含义
返回值含义及使用  **建议 直接 参考示例项目页面代码**。
```
//返回值 event：是一个JSON对象。
//event.tag : 返回值对应的含义,具体如下
switch (event.tag){
	case "locationPermissions":
		/* 位置权限弹窗后,用户操作的回调- 允许了位置权限 或者拒绝了位置权限!
		环境初始化时,传入的参数isRequestPermissions为true 有效*/	
		break;
	case "wifiData":
		/* 执行环境检查函数成功后,返回的手机连接的wifi的基本数据 */	
		break;
	case "deviceData":
		/* 配网成功回调,返回的设备数据 */	
		break;
	case "provisioningSuccess":
		/* 配网成功后,供应停止的回调 */
		break;
	case "autoProvisioningFail":
	/* 90秒后超时自动停止供应,配网失败回调! */
		break;
	case "handProvisioningFail":
		/* 手动停止配网 成功的回调 */
		break;
	case "log":
		/* 日志 */
	 break;
	default:
		console.log(event);	
		break;
}
```


## 所需权限
#### 安卓：
-  "android.permission.INTERNET",
-	"android.permission.ACCESS_WIFI_STATE",
-	"android.permission.ACCESS_NETWORK_STATE",
-	"android.permission.CHANGE_WIFI_MULTICAST_STATE",
-	"android.permission.CHANGE_WIFI_STATE",
-	"android.permission.ACCESS_FINE_LOCATION"
#### IOS：
-  "NSLocationWhenInUseUsageDescription",
-  "NSLocationAlwaysUsageDescription",
- "NSLocationAlwaysAndWhenInUseUsageDescription",
-	"NSLocalNetworkUsageDescription"

#### uniAPP打包时必要的权限配置
1. 打开项目 manifest.json 文件，点击源码视图。
2. 检查，并增加 如下distribute 节点 下的 信息。
```
        "distribute" : {
            /* android打包配置 */
            "android" : {
                "permissions" : [
                    "<uses-permission android:name=\"android.permission.INTERNET\"/>",
					"<uses-permission android:name=\"android.permission.ACCESS_WIFI_STATE\"/>",
					"<uses-permission android:name=\"android.permission.ACCESS_NETWORK_STATE\"/>",
					"<uses-permission android:name=\"android.permission.CHANGE_WIFI_MULTICAST_STATE\"/>",
					"<uses-permission android:name=\"android.permission.CHANGE_WIFI_STATE\"/>",
				    "<uses-permission android:name=\"android.permission.ACCESS_FINE_LOCATION\"/>"
                ],
                "minSdkVersion" : 23
            },
            /* ios打包配置 */
            "ios" : {
                "privacyDescription" : {
                    "NSLocalNetworkUsageDescription" : "APP需要您同意使用本地网络，才能继续使用UDP功能，用于设备配网。",
                    "NSLocationWhenInUseUsageDescription" : "APP需要您同意访问位置信息，才能获取WI-FI信息，用于设备配网。",
                    "NSLocationAlwaysUsageDescription" : "APP需要您同意访问位置信息，才能获取WI-FI信息，用于设备配网。",
                    "NSLocationAlwaysAndWhenInUseUsageDescription" : "APP需要您同意访问位置信息，才能获取WI-FI信息，用于设备配网。"

                },
                "capabilities" : {
                    "entitlements" : {
                        "com.apple.developer.networking.wifi-info" : true
                    }
                }
            }
        }
```


## 已知问题
- 在nvue页面的onUnload 生命周期函数中执行配网销毁函数 espModule.udpEnvironmentOnUnload() 时, 会出现无返回值的问题。但此问题已在插件内部解决。更为详细的原因和情况已在示例项目代码对应的NVUE页面中注释清楚。
-  不论是乐鑫的**ESP-TOUCH**或**ESP-TOUCH-V2** ，还是微信的**airkiss**其原理都是基于UDP进行的配网。 其配网过程比较依赖WIFI环境，若配网环境信道较多或杂乱数据包较多，是有可能会导致配网失败的。所以，UDP配网是不靠谱的。 若您使用的乐鑫芯片具有蓝牙能力，可考虑使用蓝牙配网方案，本人也开发了对应的插件，可在插件市场搜索**乐鑫(esp芯片)蓝牙配网插件** 。 若使用的芯片没有蓝牙能力，则优先推荐使用 **ESP-TOUCH-V2**  协议是最优的选择。
## 其他
- 本插件目前仅支持 ESP-TOUCH-V2 版本的协议。 若有需要ESP-TOUCH 版本协议的朋友，可在评论区留言，或联系 QQ： 370293265 。 若需求的朋友较多，会考虑在本插件中免费增加。
- 有问题时，可加我QQ：370293265 ，一起探讨解决。
- 添加QQ时， 请备注 **uni插件咨询** 。 




# - 乐鑫(esp芯片)蓝牙配网插件使用说明
## 简介
 本插件主要为使用 *乐鑫(esp芯片) 的嵌入式产品* 提供手机端的蓝牙配网API。需要注意嵌入式产品需遵循乐鑫SDK *Wi-Fi Provisioning* 对应的*wifi_prov_scheme_ble蓝牙配网方案*才可正常使用。[嵌入式产品SDK，Wi-Fi Provisioning 说明详见官方文档。](https://docs.espressif.com/projects/esp-idf/zh_CN/latest/esp32/api-reference/provisioning/wifi_provisioning.html)
## 功能特性
- 支持 最低 *安卓-6.0* 和 **IOS-13.0（注意IOS系统最低支持为13.0）**。
- 支持安卓和IOS插件运行所需权限，申请弹窗逻辑功能。
- 支持 乐鑫 *Wi-Fi Provisioning-wifi_prov_scheme_ble* 版协议的蓝牙配网。
- 支持 自定义 **securityType**，  **POP**，需要与嵌入式产品保持一致。
- 支持搜索蓝牙设备时根据嵌入式设备蓝牙名称的前缀进行过滤。
- **提供测试工程项目示例**，支持vue页面和nvue页面，可直接复制对应页面代码到自己的工程中，简单修改，即可正常测试使用。或者直接下载测试工程项目（若您当前在uni插件市场对应的插件说明页面，可直接在页面右上角 点击 **使用HBuiderX 导入示例项目**），简单配置，即可测试运行。*具体使用说明，详见工程代码示例中的  **README.md** 文件*。
 
## 示例工程项目源码地址
 [点击跳转示例项目源码地址](https://gitee.com/bull-demon-king-wny/uniappnativepluginexample?_from=gitee_search)
## 插件示例工程 配网页面 流程
  ```mermaid
graph LR
初始化配网环境--> 配网使用说明 --> 搜索蓝牙设备-->连接配网蓝牙设备-->搜索可用的WIFI列表-->选择WIFI开始配网-->配网环境清理
```
- 初始化配网环境:  主要检查配网所需手机权限是否正常。若不正常，则弹窗申请。
- 配网使用说明：页面文本展示，主要告知用户配网操作流程和注意事项。
- 搜索蓝牙设备： 搜索手机附近可以连接的蓝牙设备。
- 连接蓝牙设备: 选择待配网的蓝牙设备与手机建立蓝牙通讯连接。
- 搜索可用的wifi列表： 手机端获取到可用的wifi列表。
- 选择WIFI开始配网： 根据wifi列表，选择待接入的wifi，对嵌入式产品进行配网。
- 环境清理：主要用来清理配网插件运行时申请的内存或未完成的任务。

## API使用说明
### 引入--使用API之前需要先引入原生插件
```
var espModule = uni.requireNativePlugin("nw-ble-esp-provisioning_module")
```
### 使用
####  API-初始化配网环境:
```
var code = espModule.bleEnvironmentOnLoad(objectData);
```

> 入参：
> let objectData= {"isRequestPermissions":true,"securityType":0, "pop":""}
> **isRequestPermissions** : Bool值， 为true或false。 true：使用插件内的位置权限弹窗申请。 fasle：不使用插件内的位置权限弹窗申请。（鉴于安卓系统位置权限申请的逻辑问题，此值的抉择，可参考如下：若APP的其他功能模块也需要位置权限，则选择false，并在APP使用本模块之前，引导用户完成申请位置权限的功能。若APP其他模块没有使用位置权限，建议为true。 ）
> **securityType**: 值0 或者值1 ，安全通讯级别，由对应嵌入式设备得到此值。后续连接蓝牙设备API，还可再修改。
> **pop**：字符串值，通讯凭证 由对应嵌入式设备得到。securityType=1的时候 pop有效， securityType=0的时候 pop无效，传""即可。后续连接蓝牙设备API，还可再修改。
 

> 出参：
> var code： Number值，具体参见 **同步API返回code码含义** 。

> 异步回调：有
> 具体参见 **API异步返回值含义** 。

####  API-搜索蓝牙设备:
```
var code =espModule.bleStartSearchDevice(deviceNamePrefix)
```

> 入参：
> **deviceNamePrefix**：字符串值，名称前缀过滤字符串，根据搜索到的蓝牙名称匹配过滤返回对应设备。值为 "" 返回所有设备。

> 出参：
> var code： Number值，具体参见 **同步API返回code码含义** 。

> 异步回调：有
> 具体参见 **API异步返回值含义** 。

####  API-停止搜索蓝牙设备:
```
var code =espModule.bleStopSearchDevice()
```

> 入参：无

> 出参：
> var code： Number值，具体参见 **同步API返回code码含义** 。

> 异步回调：有
> 具体参见 **API异步返回值含义** 。

####  API-连接蓝牙设备:
```
var code =  espModule.bleConnectDevice(objectData);
```

> 入参：
> let objectData= {"mBleDevice":mBleDevice,"securityType":0,"pop":"" }
> **mBleDevice**：通过搜索蓝牙设备API返回的某一个蓝牙对象。
> **securityType**: 值0 或者值1 ，安全通讯级别，由对应嵌入式设备得到此值。（此值与前面的初始化环境API入参重复，但实际以此API传入的值为准。）
> **pop**：字符串值，通讯凭证，由对应嵌入式设备得到。（此值与前面的初始化环境API入参重复，但实际以此API传入的值为准。）

> 出参：
> var code： Number值，具体参见 **同步API返回code码含义** 。

> 异步回调：有
> 具体参见 **API异步返回值含义** 。

####  API-停止正在连接或断开已经连接的蓝牙设备:
```
var code =  espModule.bleStopDisconnectDevice();
```

> 入参：无

> 出参：
> var code： Number值，具体参见 **同步API返回code码含义** 。

> 异步回调：有
> 具体参见 **API异步返回值含义** 。

####  API-获取WIFI列表:
```
var code =  espModule.bleScanNetworks();
```

> 入参：无

> 出参：
> var code： Number值，具体参见 **同步API返回code码含义** 。

> 异步回调：有
> 具体参见 **API异步返回值含义** 。

####  API-开始蓝牙配网:
```
var code =espModule.bleStartProvisioning(objectData);
```

> 入参：let objectData= {"wifiName":mWifi.wifiName,"security":mWifi.security,"passWord":passWord}
> **wifiName**：通过获取WIFI列表API返回的对应单个WIFI的名称。
 > **security**：通过获取WIFI列表API返回的对应单个WIFI的加密安全级别，规定级别为0时，不需要wifi密码，传""即可。
 >**passWord**：用户输入的当前WIFI的密码。 

> 出参：
> var code： Number值，具体参见 **同步API返回code码含义** 。

> 异步回调：有
>具体参见 **API异步返回值含义** 。
####  API-停止蓝牙配网:
```
var code = espModule.bleStopProvisioning();
```

> 入参：无 

> 出参：
> var code： Number值，具体参见 **同步API返回code码含义** 。

> 异步回调：有
>具体参见 **API异步返回值含义** 。

####  API-环境清理:
```
var code = espModule.bleEnvironmentOnUnload();
```

> 入参：无 

> 出参：
> var code： Number值，具体参见 **同步API返回code码含义** 。

> 异步回调：有
>具体参见 **API异步返回值含义** 。

### API同步返回code码含义
```
let codeState = [
	{
		code:0,
		message:"执行成功!"
		
	},
	{
		code:1,
		message:"环境已存在,不需要重复初始化!"
		
	},
	{
		code:2,
		message:"配网环境未得到初始化,请先执行环境初始化!"
		
	},
	{
		code:3,
		message:"搜索蓝牙任务,已经停止运行,无需重复停止!"
		
	},
	{
		code:4,
		message:"搜索蓝牙任务,已经在运行,无需重复运行!"
		
	},
	{
		code:5,
		message:"蓝牙设备不具备扫描WIFI列表的能力!"
		
	},
	{
		code:6,
		message:"蓝牙设备未连接,请先连接设备!"
		
	},
	{
		code:7,
		message:"传递参数错误!"
		
	},
	{
		code:8,
		message:"初始化会话失败,请重新初始化!"
		
	},
	{
		code:9,
		message:"预留!"
		
	},
	{
		code:10,
		message:"环境检查OK!"  //插件使用者无需关注
		
	},
	{
		code:11,
		message:"位置权限未打开!" //参数isRequestPermissions 为false有效
		 
	},
	{
		code:12,
		message:"GPS未打开!"
		
	},
	{
		code:13,
		message:"此设备不支持蓝牙!"
		
	},
	{
		code:14,
		message:"需要打开蓝牙开关,才能继续!"  //检测到蓝牙未开启,已经向用户请求弹窗
		
	},
	/* 需要注意 若参数isRequestPermissions为true, 请保证不要在系统的其他地方调用 申请位置权限弹窗的操作,否则回应码将是不准确的! */
	{
		code:15,
		message:"需要位置权限,才能继续!" //参数isRequestPermissions 为true有效  (第一次弹出的位置权限弹窗,被用户手动拒绝了,需要手动引导用户)
		
	},
	{
		code:16,
		message:"请允许位置权限,才能继续!" //参数isRequestPermissions 为true有效 ( 位置权限第一次弹窗成功,需要监听用户操作后的回调locationPermissions)
		
	},
	{
		code:17,
		message:"需要位置权限,才能继续!" //  不准确的!--理论上不会出现这个回应码,若出现则说明系统在其他位置也调用了申请位置权限弹窗的操作! 参数isRequestPermissions 为true有效
		
	},
	{
		code:18,
		message:"正在连接蓝牙设备,请允许配对或稍后重试!!"
		
	},
	{
		code:19,
		message:"已经连接蓝牙设备,请断开蓝牙后重新尝试!" 
		
	},
	{
		code:20,
		message:"正在供应配网数据,稍后重试!" 
		
	},
	{
		code:21,
		message:"需要蓝牙权限,才能继续!" //蓝牙权限被用户拒绝,需要手动引导用户!--IOS有效
		
	},
	{
		code:22,
		message:"需要蓝牙权限,才能继续!"  //蓝牙权限申请已弹窗,监听回调!--目前IOS有效
		
	}
	
]
```
### API异步返回值含义
#### 注册回调事件
注册回调事件，才能收到异步返回值。**建议 直接 参考示例项目页面代码**。
```
var globalEvent = uni.requireNativePlugin('globalEvent'); //插件全局监听

/* 注册插件监听事件,页面内有效 */
globalEvent.addEventListener('espEvent', function(event) {
		console.log('收到插件espEvent事件:'+JSON.stringify(event));
	 });
```
#### 返回值含义
返回值含义及使用  **建议 直接 参考示例项目页面代码**。
```
//返回值 event：是一个JSON对象。
//event.tag : 返回值对应的含义,具体如下
switch (event.tag){
	case "locationPermissions":{
			/* 位置权限弹窗后,用户操作的回调- 允许了位置权限 或者拒绝了位置权限!
		环境初始化时,传入的参数isRequestPermissions为true 有效*/	
			break;
		}
	case "blePermissions":{
			/* 蓝牙关闭的情况下,会向用户弹窗请求打开蓝牙, 这里是用户操作后的回调! */
			break;	
	}
	case "blePermissionsState" :{
		// 蓝牙权限用户操作后的 回调---只IOS有效

		break;	
	}
	case "mBleDevice":{
		/* 扫描蓝牙设备,返回的蓝牙设备 */

			break;	
	}
	case "wifiList":{	
		/* 搜索wifi列表获取到的WIFI列表 */
		break;	
	}
	case "bleScanListenerCodeState":{
		/* 蓝牙设备扫描监听 回调状态 */
		break;	
	}
	case "bleConnectCodeState":{
		/* 蓝牙设备首次连接 回调状态 */
		break;	
	}
	case "initSessionCodeState":{	
		/* 蓝牙设备首次建立会话 回调状态 */
			break;	
	}
	case "provisioningCodeState":{
		/* 配网API调用后,成功或失败后的回调码 */
			break;	
	}
	default:
		console.log(event);	
		break;
}

```

## 所需权限
#### 安卓：

 - "android.permission.BLUETOOTH",
 - "android.permission.BLUETOOTH_ADMIN",
 - "android.permission.INTERNET",
 - "android.permission.ACCESS_WIFI_STATE",
 - "android.permission.ACCESS_NETWORK_STATE",
 - "android.permission.CHANGE_WIFI_MULTICAST_STATE",
 - "android.permission.CHANGE_WIFI_STATE",
 - "android.permission.ACCESS_FINE_LOCATION"
 - "android.permission.CAMERA"

#### IOS：
- "NSLocationWhenInUseUsageDescription",
- "NSLocationAlwaysUsageDescription",
- "NSLocationAlwaysAndWhenInUseUsageDescription",
- "NSBluetoothAlwaysUsageDescription",
- "NSCameraUsageDescription" //**需要说明：相机权限，本插件API暂未使用相机权限，但插件中引用的三方SDK中使用相机API，用于扫码联网功能。所以在uniAPP打包时必须配置 使用描述语，否则APP将审核不通过，具体配置见下节  uniAPP打包时必要的权限配置**

#### uniAPP打包时必要的权限配置
1. 打开项目 manifest.json 文件，点击源码视图。
2. 检查，并增加 如下distribute 节点 下的 信息。
```
        "distribute" : {
            /* android打包配置 */
            "android" : {
                "permissions" : [
                    "<uses-permission android:name=\"android.permission.INTERNET\"/>",
					"<uses-permission android:name=\"android.permission.ACCESS_WIFI_STATE\"/>",
					"<uses-permission android:name=\"android.permission.ACCESS_NETWORK_STATE\"/>",
					"<uses-permission android:name=\"android.permission.CHANGE_WIFI_MULTICAST_STATE\"/>",
					"<uses-permission android:name=\"android.permission.CHANGE_WIFI_STATE\"/>",
				    "<uses-permission android:name=\"android.permission.ACCESS_FINE_LOCATION\"/>",
				    "<uses-permission android:name=\"android.permission.BLUETOOTH\"/>",
				    "<uses-permission android:name=\"android.permission.BLUETOOTH_ADMIN\"/>",
				    "<uses-permission android:name=\"android.permission.CAMERA\"/>"
                ],
                "minSdkVersion" : 23
            },
            /* ios打包配置 */
            "ios" : {
                "privacyDescription" : {
                    "NSBluetoothAlwaysUsageDescription" : "APP需要蓝牙权限，才能进行蓝牙通讯,用于设备配网。",
                    "NSLocationWhenInUseUsageDescription" : "APP需要您同意访问位置信息，才能获取WI-FI信息，用于设备配网。",
                    "NSLocationAlwaysUsageDescription" : "APP需要您同意访问位置信息，才能获取WI-FI信息，用于设备配网。",
                    "NSLocationAlwaysAndWhenInUseUsageDescription" : "APP需要您同意访问位置信息，才能获取WI-FI信息，用于设备配网。"
					"NSCameraUsageDescription" : "APP需要访问您的相机以便拍照或录制视频，用于在应用中分享或扫描设备。我们不会在您未经授权的情况下访问您的相机。"
                },
                "capabilities" : {
                    "entitlements" : {
                        "com.apple.developer.networking.wifi-info" : true
                    }
                }
            }
        }
```


## 已知问题
- 在nvue页面的onUnload 生命周期函数中执行配网销毁函数 espModule.bleEnvironmentOnUnload() 时, 会出现无返回值的问题。但此问题已在插件内部解决。更为详细的原因和情况已在示例项目代码对应的NVUE页面中注释清楚。
- 在IOS手机，若第一次系统蓝牙权限弹窗被用户拒绝后，后续通过自定义弹窗跳转到手机的蓝牙权限设置页面后，用户操作蓝牙权限状态后，将会导致APP重启。-----**此问题暂未找到原因，也找了几个需要蓝牙权限的其他APP测试，同样也会有此问题，所以可能是IOS系统自有问题**。

## 常见问题
 - 可以搜索到蓝牙设备,但是点击蓝牙设备进行连接时,提示连接失败.
   **  这个问题可能是如下原因: **
       1. ESP支持两种蓝牙配网协议分别为ESP Blufi 协议和ESP BLE provisioning协议， 本插件使用的是ESP BLE provisioning ，对应的嵌入式设备应保持一致。
       2. ESP BLE provisioning协议 支持V0，V1，V2三种加密方案，本插件只支持V0和V1，如果嵌入式设备生成的QRCODE码中包含有 username ，即为V2版本， 本插件目前仅支持V0和V1版本的通讯，即QRCODE码不能包含username字段。

## 其他
- 本插件目前仅支持 乐鑫 *Wi-Fi Provisioning-wifi_prov_scheme_ble* 版协议的蓝牙配网，若有需要 *wifi_prov_scheme_softap* 版协议配网插件的，可在评论区留言或联系QQ：370293265。 若需要的朋友较多，可在本插件中免费增加。
- 本插件目前暂未支持扫设备码联网的能力，若有需要的朋友可联系本人，若需求较多，一样可在本插件中免费增加。
- 有问题或需求时，可加我QQ：370293265 ，一起探讨解决。
- 添加QQ时， 请备注 **uni插件咨询** 。 
